# web_routes.py
from flask import Blueprint, render_template, redirect, url_for, session, request
import sqlite3
from utils import hash_password
web_blueprint = Blueprint('web', __name__)

# Register route
@web_blueprint.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        # Hash password
        hashed_password = hash_password(password)

        # Check if username already exists
        conn = sqlite3.connect('auth_database.db')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM users WHERE username = ?', (username,))
        existing_user = cursor.fetchone()
        if existing_user:
            conn.close()
            return render_template('register.html', error="Username already exists.")
        
        # Insert new user
        cursor.execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, hashed_password))
        conn.commit()
        conn.close()
        
        return redirect(url_for('web.login'))

    return render_template('register.html')


# Login route
@web_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        # Hash password
        hashed_password = hash_password(password)

        # Check if username and password match
        conn = sqlite3.connect('auth_database.db')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM users WHERE username = ? AND password = ?', (username, hashed_password))
        user = cursor.fetchone()
        conn.close()

        if user:
            session['username'] = username
            return redirect(url_for('web.index'))
        else:
            return render_template('login.html', error="Invalid username or password.")
    
    return render_template('login.html')

# Logout route
@web_blueprint.route('/logout', methods=['POST'])
def logout():
    session.pop('username', None)
    return redirect(url_for('web.index'))

# Index route
@web_blueprint.route('/', methods=['GET'])
def index():
    if 'username' in session:
        return render_template('index.html', username=session['username'])
    else:
        return redirect(url_for('web.login'))

# Graph route
@web_blueprint.route('/graph', methods=['GET', 'POST'])
def graph():
    if 'username' not in session:
        return redirect(url_for('web.login'))

    num_values = 10  # default value
    if request.method == 'POST':
        num_values = int(request.form['num_values'])

    conn = sqlite3.connect('temp_database.db')
    cursor = conn.cursor()
    cursor.execute('SELECT value, timestamp FROM temperature ORDER BY timestamp DESC LIMIT ?', (num_values,))
    data = cursor.fetchall()
    conn.close()

    # Prepare data for plotting
    timestamps = [row[1] for row in data]
    values = [row[0] for row in data]

    # Render the graph template
    return render_template('graph.html', timestamps=timestamps, values=values, num_values=num_values)
