# main.py
from flask import Flask
from api_routes import api_blueprint
from web_routes import web_blueprint
from utils import hash_password
import sqlite3  # Add this import statement

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

app.register_blueprint(api_blueprint)
app.register_blueprint(web_blueprint)

# Database setup
def init_databases():
    init_auth_db()
    init_temp_db()

def init_auth_db():
    conn = sqlite3.connect('auth_database.db')
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            username TEXT UNIQUE NOT NULL,
            password TEXT NOT NULL
        )
    ''')
    conn.commit()
    conn.close()

def init_temp_db():
    conn = sqlite3.connect('temp_database.db')
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS temperature (
            id INTEGER PRIMARY KEY,
            value REAL NOT NULL,
            timestamp TEXT NOT NULL,
            user_id INTEGER,
            FOREIGN KEY(user_id) REFERENCES users(id)
        )
    ''')
    conn.commit()
    conn.close()

if __name__ == '__main__':
    init_databases()
    app.run(debug=True)
