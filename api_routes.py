from flask import Blueprint, jsonify, request, session
import sqlite3
import datetime
import random

api_blueprint = Blueprint('api', __name__)

# Add temperature route
@api_blueprint.route('/api/add_temperature', methods=['POST'])
def add_temperature():
    if 'username' in session:
        value = round(random.uniform(10.0, 30.0), 2)
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        conn = sqlite3.connect('temp_database.db')
        cursor = conn.cursor()
        cursor.execute('INSERT INTO temperature (value, timestamp, user_id) VALUES (?, ?, ?)', (value, timestamp, 1))
        conn.commit()
        conn.close()

        return jsonify({'message': 'Temperature added successfully'}), 201
    else:
        return jsonify({'error': 'Unauthorized access'}), 401

# Show temperatures route
@api_blueprint.route('/api/show_temperatures', methods=['POST'])
def show_temperatures():
    if 'username' in session:
        num_values = int(request.json['num_values'])

        conn = sqlite3.connect('temp_database.db')
        cursor = conn.cursor()
        cursor.execute('SELECT value, timestamp FROM temperature ORDER BY id DESC LIMIT ?', (num_values,))
        temperatures = cursor.fetchall()
        conn.close()

        return jsonify({'temperatures': temperatures}), 200
    else:
        return jsonify({'error': 'Unauthorized access'}), 401

# Delete temperatures route
@api_blueprint.route('/api/delete_temperatures', methods=['POST'])
def delete_temperatures():
    if 'username' in session:
        num_values = int(request.json['num_values'])

        conn = sqlite3.connect('temp_database.db')
        cursor = conn.cursor()
        cursor.execute('DELETE FROM temperature WHERE id IN (SELECT id FROM temperature ORDER BY id ASC LIMIT ?)', (num_values,))
        conn.commit()
        conn.close()

        return jsonify({'message': 'Temperatures deleted successfully'}), 200
    else:
        return jsonify({'error': 'Unauthorized access'}), 401
